#!/bin/sh

set -e # If a command fails, return a failure

# Env substitute 
# Syntax envsubst < template to pass > New output file
envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf

# Start nginx
# Docker needs to run nginx with the daemon off
    # This way docker can read outputs from nginx 
nginx -g 'daemon off;'